const {
  Sequelize, DataTypes 
} = require('sequelize');
const dbConfig = require('./db');

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: config.dbConn.dbServer,
  dialect: "postgres",
  port: 5432,
  logging: false,
  define: {
      timestamps: false,
  },
  timezone: "Asia/Kolkata"
});


module.exports = sequelize;
