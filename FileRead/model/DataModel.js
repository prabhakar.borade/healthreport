//  var connect = require("../Config/dbConnection");

// var sequelize = connect.sequelize;
// var Sequelize = connect.Sequelize;
// const Model = connect.Sequelize.Model;
const sequelizeTransforms = require('sequelize-transforms');
const sequelizePaginate = require('sequelize-paginate');


// class ImportExcel extends Model{}




// module.exports.ImportExcel = function() {
//     ImportExcel.init({
//   id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
//   Name: { type: Sequelize.STRING, allowNull: true },
//   Symbol: { type: Sequelize.STRING, allowNull: false },
//   Exchange: { type: Sequelize.STRING, allowNull: true },
//   Price: {type: Sequelize.DECIMAL, allowNull: false},
//   Market_Cap: { type: Sequelize.DECIMAL, allowNull: true },
//   Annualized_revenue: { type:Sequelize.DECIMAL, allowNull: true },
//   Forward_Revenue: { type:Sequelize.DECIMAL, allowNull: false},
//   Efficiency: { type:Sequelize.STRING, allowNull: false},
//   Revenue_Growth_Rate: { type: Sequelize.STRING, allowNull: false },
//   Gross_Margin:{ type: Sequelize.STRING, allowNull: false},
//   LTM: { type: Sequelize.DECIMAL, allowNull: false },
//   Date:{ type: Sequelize.STRING, allowNull: false },
//     },
//     {
//         sequelize,
//         modelName: "ImportExcel",
//         tableName: "ImportExcel",
//     });
//     return ImportExcel;
// };


// let ImportExcel = sequelize.define('importExcel',{
//     id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
//     Name: { type: Sequelize.STRING, allowNull: true },
//     Symbol: { type: Sequelize.STRING, allowNull: false },
//     Exchange: { type: Sequelize.STRING, allowNull: true },
//     Price: {type: Sequelize.DECIMAL, allowNull: false},
//     Market_Cap: { type: Sequelize.DECIMAL, allowNull: true },
//     Annualized_revenue: { type:Sequelize.DECIMAL, allowNull: true },
//     Forward_Revenue: { type:Sequelize.DECIMAL, allowNull: false},
//     Efficiency: { type:Sequelize.STRING, allowNull: false},
//     Revenue_Growth_Rate: { type: Sequelize.STRING, allowNull: false },
//     Gross_Margin:{ type: Sequelize.STRING, allowNull: false},
//     LTM: { type: Sequelize.DECIMAL, allowNull: false },
//     Date:{ type: Sequelize.STRING, allowNull: false },
//   });
//   sequelizeTransforms(ImportExcel);
//   sequelizePaginate.paginate(ImportExcel);
//   module.exports = ImportExcel;