// var dataconn = require('./Data/DataConnection');
var datamodel = require('./model/DataModel');
// var dataaccess = require('./Data/DataAccess');

var express = require('express');
var path = require('path');
var fs = require('fs');
var app = express();
var staticRoot = __dirname + './../PartnerPortal-UI/dist/'; 
//var publicPath = path.join(__dirname, 'public');
var config = require('./Config'); 
const models = require('./model');

// models.sequelize.sync({}).then(()=>{
//   const allRegisterRoutes = listEndpoints(app);
//   seeder(allRegisterRoutes);
// });
// const xlsxFile = require('read-excel-file/node');

// xlsxFile('./File/HealthReport.xlsx').then((rows) => {
//     console.log("Test Rows", rows);
//     //console.table(rows);
//     })
var XLSX = require('xlsx');
app.use(express.static('public'));
app.set('views', __dirname + '/public');
app.engine('html', require('ejs').renderFile);

var publicPath = path.join(__dirname, 'Upload');




app.use(express.static('public'));
app.set('views', __dirname + '/public');
app.engine('html', require('ejs').renderFile);
// end 

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({ limit: '50mb' }));

// CORS Middleware node.js package for connect express
app.use(function(req, res, next) {

    //enabling CORS
    var menthods = "GET, POST";
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", menthods);
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, contentType, Content-Type, Accept, Authorization");
    if (!menthods.includes(req.method.toUpperCase())) {
        return res.status(200).json({});
    };
    next();
});

app.use(express.static(staticRoot));

////#region Common Functions 

// Service checking method
app.get("/excelData", function(req, res) {
    var workbook = XLSX.readFile(config.excelFilePath, {'type': "file", cellDates: true});
var sheet_name_list = workbook.SheetNames;
var excelData = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]])
//console.log("excelData", excelData)

// fs.readFile('./people.json', 'utf8', function (err,excelData) {
//     data = JSON.parse(excelData); // you missed that...
//     console.log("Name:", data);
//     for(var i = 0; i < data.length; i++) {
//     }
//   });
// const ImportExcel = datamodel.ImportExcel();
//             var values = {
//                 Name: excelData[0]["Name"],
//                 Symbol: excelData[0]["Symbol"],
//                 Exchange: excelData[0]["Exchange"],
//                 Price: excelData[0]["Price"],
//                 Market_Cap: excelData[0]["'Market Cap (M)'"],
//                 Annualized_revenue: excelData[0]["'EV/Forward Revenue'"],
//                 Forward_Revenue: excelData[0]["'EV/Forward Revenue'"],
//                 Efficiency: excelData[0]["Efficiency"],
//                 Revenue_Growth_Rate: excelData[0]["'Revenue Growth Rate'"],
//                 Gross_Margin: excelData[0]["'Gross Margin'"],
//                 LTM: excelData[0]["LTM"],
//                 Date: excelData[0]["Date"],
//             };
//             dataaccess.Create(ImportExcel, values)
//                 .then(function (result) {
//                     if (result != null) {
//                         //res.status(200).json({ Success: true, Message: 'Excel data saved successfully', Data: result });
//                     }
//                     else {
//                         //res.status(200).json({ Success: false, Message: 'Error occurred while saving record', Data: null });
//                     }
//                 }, function (err) {
//                     //res.status(200).json({ Success: false, Message: 'Error occurred while saving record', Data: null });
//                 });

    res.status(200).send({ Success: true,  Data: excelData });
});


app.get("/sample", function(req, res) {
    res.status(200).json({ Success: true, Message: "Welcome Hello ", Data: null });
});
// Connection checking method
app.get("/CheckConnection", function(req, res) {
    dataconn.CheckConnection(res);
});

//Table creation Method
app.get("/CreateTable", function(reg, res) {
    dataconn.CreateTable(res);
});

//View Files
app.get("/ViewFile/:FolderName/:Id/:filename", (req, res) => { 
    res.sendFile(path.join(publicPath,  req.params.FolderName, req.params.Id, req.params.filename));
});




// app.post("/PaymentResponse", function(req, res) {
//     console.log('sample request');
//    // console.log(req);
//     payment.PaymentResponse(req, res);
//    // res.status(200).json({ Success: true, Message: "Welcome Hello ", Data: null });
// });

//PAyment Gateway End

// Catch all other routes and return the index file
app.get('*', (req, res) => {
    res.sendFile(path.join(staticRoot, 'index.html'));
});

//Start server and listen on http://localhost:1339/
var server = app.listen(config.service_port, function() {
    console.log('sachin ',  server.address());
    var host = server.address().address;
    var port = config.service_port ;//server.address().port;
    var datetime = new Date();
    var message = "Server :- " + host + " running on Port : - " + port + " Started at :- " + datetime;
    console.log(message);
});

// var server = https.createServer(options,app).listen(config.service_port, () => {

//     var host = server.address().address;
//     var port = server.address().port;
//     var datetime = new Date();
//     var message = "Server :- " + host + " running on Port : - " + port + " Started at :- " + datetime;
//     console.log(message);
// });