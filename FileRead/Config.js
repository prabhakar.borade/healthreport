const { fstat } = require("fs");


var environmentConfig = {
    local: {
        service_port: 1339,
        ui_url: 'http://localhost:4200/',
        group_mail: '',
        uploadfileConfig:{
            folder_name: 'Upload',
            file_path: 'localpath',
        },
        emailConfig: {
            email_host: '10.250.6.37',
            from_email: 'sachin.pawale@neweltechnologies.com',
        },
        dbConn: {
            dbServer: 'localhost',
            dbName: 'HealthReport',
            dbUser: 'postgres',
            dbPassword: 'India@123',
        }
    },
    sit: {
        service_port: 1337,
        ui_url: 'http://edemumnewuatvm4:1337/',
        group_mail: '',
        uploadfileConfig:{
            folder_name: 'Upload-UAT',
            file_path: 'localpath',
        },
        emailConfig: {
            email_host: '10.250.6.63',
            from_email: '',
        },
        dbConn: {
            dbServer: '10.250.19.84',
            dbName: 'BSFL',
            dbUser: 'BSFLuser',
            dbPassword: 'BSFLuser~',
        }
    },
    uat: {
        service_port: 1338,
        ui_url: 'https://partnerportaluat.bajajfinservsecurities.in',
        group_mail: '',
        uploadfileConfig:{
            folder_name: 'Upload-UAT',
            file_path: 'localpath',
        },
        emailConfig: {
            email_host: '192.168.43.117',
            from_email: 'sachin.pawale@neweltechnologies.com',
        },
        dbConn: {
            dbServer: 'localhost',
            dbName: 'BFSL',
            dbUser: 'postgres',
            dbPassword: '1234',
        }
    },
    live: {
        service_port: 1337,
        ui_url: 'http://edemumkalapp035:1337/',
        group_mail: '',
        uploadfileConfig:{
            folder_name: 'Upload-UAT',
            file_path: 'localpath',
        },
        emailConfig: {
            email_host: '10.250.6.63',
            from_email: 'sachin.pawale@neweltechnologies.com',
        },
        dbConn: {
            dbServer: '10.250.0.237',
            dbName: 'BSFL',
            dbUser: 'BSFL',
            dbPassword: 'BSFL~',
        }
    }
}

var environment = 'local';

const finalConfig = environmentConfig[environment];

module.exports.service_port = finalConfig.service_port;
module.exports.ui_url = finalConfig.ui_url;
module.exports.group_mail = finalConfig.group_mail;
module.exports.emailConfig = finalConfig.emailConfig;
module.exports.dbConn = finalConfig.dbConn;
module.exports.uploadfileConfig = finalConfig.uploadfileConfig;

module.exports.gmailFromEmail = 'neweltechnologies111@gmail.com';
module.exports.gmailToEmail = 'sachin.pawale@neweltechnologies.com;rg050904@gmail.com';

module.exports.gmailUser = 'neweltechnologies111@gmail.com';
module.exports.gmailPass = 'Newel@1234';

// Payment Gateway Live Keys
module.exports.accessCode = 'AVQG05IC21BY39GQYB';
module.exports.workingKey= 'FB18BFB5C920927FC0379042EEB00780';
module.exports.excelFilePath= './File/HealthReport.xlsx';